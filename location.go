package ipgeolocation

// Location represents a place associated with an ip address.
type Location struct {
	IP           string `json:"ip"`
	HostName     string `json:"hostname"`
	CountryCode2 string `json:"country_code2"`
	CountryCode3 string `json:"country_code3"`
	Country      string `json:"country_name"`
	City         string `json:"city"`
	Postcode     string `json:"zipcode"`
	Latitude     string `json:"latitude"`
	Longitude    string `json:"longitude"`
}

/*
Example result for 8.8.8.8

{
    "ip": "8.8.8.8",
    "hostname": "google-public-dns-a.google.com",
    "continent_code": "NA",
    "continent_name": "North America",
    "country_code2": "US",
    "country_code3": "USA",
    "country_name": "United States",
    "country_capital": "Washington",
    "state_prov": "California",
    "district": "",
    "city": "Mountain View",
    "zipcode": "94043",
    "latitude": "37.4229",
    "longitude": "-122.085",
    "is_eu": false,
    "calling_code": "+1",
    "country_tld": ".us",
    "languages": "en-US,es-US,haw,fr",
    "country_flag": "https://ipgeolocation.io/static/flags/us_64.png",
    "isp": "Level 3 Communications",
    "connection_type": "",
    "organization": "Google Inc.",
    "geoname_id": "5375480",
    "currency": {
        "code": "USD",
        "name": "US Dollar",
        "symbol": "$"
    },
    "time_zone": {
        "name": "America/Los_Angeles",
        "offset": -8,
        "current_time": "2019-01-14 03:30:00.135-0800",
        "current_time_unix": 1547465400.135,
        "is_dst": false,
        "dst_savings": 1
    }
}
*/
