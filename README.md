# ipgeolocation

Package ipgeolocation provides location information for an ip address using the ipgeolocation.io api. You need to get an apikey from them first at https://ipgeolocation.io/

## usage

Common usage example:

    ipgeolocation.SetAPIkey("[[my-key-here]]")
    loc, err := ipgeolocation.Get("[[ip-address-here]]")
    if err != nil {
        log.Print(err)
    }
    // Do something with loc here.

