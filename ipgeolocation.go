package ipgeolocation

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

var apikey = ""

// SetAPIkey sets the https://ipgeolocation.io/ api key to be used.
func SetAPIkey(key string) {
	apikey = key
}

// Get returns a location object for the specified ip address from the https://ipgeolocation.io/ api. You need to set the API key first.
func Get(ipaddress string) (*Location, error) {
	if apikey == "" {
		return nil, errors.New("apikey was not set, call SetAPIkey() first")
	}
	if ipaddress == "::1" || ipaddress == "127.0.0.1" {
		return nil, errors.New("cannot get geo location for localhost")
	}
	url := "https://api.ipgeolocation.io/ipgeo?apiKey=" + apikey + "&ip=" + ipaddress
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		return nil, errors.New("could not get geo location for ip address " + ipaddress)
	}
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var location Location
	if err := json.Unmarshal(data, &location); err != nil {
		return nil, err
	}
	return &location, nil
}
